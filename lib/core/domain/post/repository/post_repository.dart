import 'package:flutter_base/core/domain/post/entity/post_entity.dart';

abstract class PostRepository{
  Future<List<ResPost>> getPosts();
}