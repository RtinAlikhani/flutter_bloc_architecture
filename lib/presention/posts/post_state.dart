import 'package:flutter_base/data/model/post_model.dart';

abstract class PostState{
  const PostState();
}

class IsLoadingState extends PostState{
  final bool isLoading;

  IsLoadingState(this.isLoading);
}

class PostsLoadedState extends PostState{
  final List<PostModel> posts;

  PostsLoadedState(this.posts);
}

class PostInitState extends PostState{
}