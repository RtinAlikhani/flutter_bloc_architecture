import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/core/domain/post/usecase/get_post_usecase.dart';
import 'package:flutter_base/data/model/post_model.dart';
import 'package:flutter_base/presention/posts/post_state.dart';

class PostCubit extends Cubit<PostState> {
  final GetPostUseCase getPostUseCase;

  PostCubit(this.getPostUseCase) : super(PostInitState());

  Future<List<PostModel>> getPosts() async {
    emit(IsLoadingState(true));
    var response = await getPostUseCase.call();
    print(response);
    emit(IsLoadingState(false));
    emit(PostsLoadedState(response));
    return response;
  }
}
