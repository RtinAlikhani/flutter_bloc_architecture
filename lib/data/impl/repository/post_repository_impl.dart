

import 'package:flutter_base/core/domain/post/entity/post_entity.dart';
import 'package:flutter_base/core/domain/post/repository/post_repository.dart';
import 'package:flutter_base/data/datasource/post_datasource.dart';

class PostRepositoryImpl implements PostRepository {
  final PostDataSourceRemote postDataSourceRemote;

  PostRepositoryImpl(this.postDataSourceRemote);

  @override
  Future<List<ResPost>> getPosts() async {
    try {
      var response= await postDataSourceRemote.getPosts();
      return response;
    } catch (e) {
      throw e;
    }
  }
}
