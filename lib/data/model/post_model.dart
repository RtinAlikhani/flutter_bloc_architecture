import 'dart:convert';

import 'package:flutter_base/core/domain/post/entity/post_entity.dart';

List<PostModel> resPostFromJson(Map<String, dynamic> json) => List<PostModel>.from(json["items"].map((x) => PostModel.fromJson(x)));

//List<PostModel> resPostFromJson(String str) => List<PostModel>.from(json.decode(str).map<PostModel>((x) => PostModel.fromJson(x)));
String resPostToJson(List<PostModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PostModel extends ResPost {
  PostModel({
    this.userId,
    this.id,
    this.title,
    this.body,
  });

  int userId;
  int id;
  String title;
  String body;

  factory PostModel.fromJson(Map<String, dynamic> json) => PostModel(
    userId: json["userId"],
    id: json["id"],
    title: json["title"],
    body: json["body"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId,
    "id": id,
    "title": title,
    "body": body,
  };
}