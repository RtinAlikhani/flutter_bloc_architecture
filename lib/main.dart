import 'package:flutter/material.dart';
import 'package:flutter_base/data/model/post_model.dart';
import 'package:flutter_base/presention/posts/post_cubit.dart';
import 'package:flutter_base/presention/posts/post_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'di/container_module.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  init();
  runApp(MaterialApp(home:MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => get<PostCubit>(),
      child: BlocBuilder<PostCubit, PostState>(
        builder: (context, state) {
          if(state is PostsLoadedState){
            return _itemList(state.posts);
          }else if(state is IsLoadingState){
            if(state.isLoading) return _progressbar();
          }else {
            _getPosts(context);
            return _progressbar();}
        },
      ),
    );
  }


  Widget _row(PostModel post) {
    return ListTile(
      title: Text(
        post.title,
        style: TextStyle(color: Colors.deepOrange,fontSize: 18),
      ),
      subtitle: Text(post.body),
      leading: FlutterLogo(),
    );
  }

  _getPosts(context) {
    BlocProvider.of<PostCubit>(context).getPosts();
  }

  Widget _itemList(List<PostModel> posts) {
    return Scaffold(
        body: Container(
          color: Colors.white,
          child: ListView.builder(
            itemCount: posts.length,
            itemBuilder: (context, index) {
              return _row(posts[index]);
            },
          ),
        ));

  }

  Widget _progressbar() {
    return Scaffold(
      body: Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.deepOrange)),
            Text("Loading . . .",style: TextStyle(color: Colors.black,fontSize: 20),)
          ],
        ),
      ),
    );

  }
}
